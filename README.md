# zig-map-reduce
## Goals
* Learn about distributed systems
* Create a very basic implementation of MapReduce
* Learn Zig

## MapReduce
Basic principle with word count as an example. Key k can be a file and Value v is the contents of the file.

```
map(k, v)
    split v into words
    for each word w
        emit(w, "1")

Reduce(k,v[])
    emit(len(v)) -->
```

## Project status
### 2024-11-24

As of right now the application only accepts a folder with text files, spawns a worker that prints the filename (with path) and each files content.

## Running the code
Create a folder, such as data/, and add some text files to it. Then run the following command.

```
$ zig build run -- data/
```
## Run tests
```
$ zig build test
```
## Run performance stats
```
$ zig build -Doptimize=ReleaseFast
$ perf stat -r 10 -d ./zig-out/bin/zig-map-reduce -- data/
```
