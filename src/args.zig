const std = @import("std");
const builtin = @import("builtin");
const mem = std.mem;
const process = std.process;

pub const Args = struct {
    const Self = @This();

    allocator: mem.Allocator,
    items: [][:0]u8,

    pub fn init(allocator: mem.Allocator) !Args {
        return Args{
            .allocator = allocator,
            .items = try process.argsAlloc(allocator),
        };
    }

    pub fn deinit(self: Self) void {
        defer process.argsFree(self.allocator, self.items);
    }

    pub fn print(self: Self) void {
        for (self.items, 0..) |item, i| {
            std.debug.print("arg[{d}]={s}\n", .{ i, item });
        }
    }
};
