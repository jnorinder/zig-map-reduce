const std = @import("std");
const ziglyph = @import("ziglyph");
const zigstr = @import("zigstr");
const letter = ziglyph.letter;
const mem = std.mem;
const fs = std.fs;
const heap = std.heap;
const math = std.math;
const debug = std.debug;
const unicode = std.unicode;
const builtin = @import("builtin");
const testing = std.testing;
const env = @import("args.zig");
const worker = @import("worker.zig");
const thread = std.Thread;

const USAGE_TEXT = "Usage: provide folder as argument, e.g., zig build run -- data/";
const DELIMITER_CHARS: []const u8 = " !\"#%&/()=?$£¡@€{[]}\\±,.-+;:0123456789|<>_";

const ArrayList = std.ArrayList;

const WordMap = struct {
    word: []const u8,
    value: []const u8,
};

const CombinedMap = struct { 
    key: []const u8, 
    value: usize, 
};

pub fn spawnWorker(allocator: mem.Allocator, path: []const u8) void {
    var w = try worker.Worker.init(.{ .allocator = allocator }, true);
    defer w.deinit();
    if (w.run(path)) |_| {} else |_| {} //TODO: perhaps handle possible errors?
}

pub fn main() !void {
    var arena = heap.ArenaAllocator.init(heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const args = try env.Args.init(allocator);
    defer args.deinit();

    if (args.items.len < 2) {
        debug.print("{s}\n", .{USAGE_TEXT});
    } else {
        var word_list = ArrayList(WordMap).init(allocator);
        defer {
            word_list.clearAndFree();
            word_list.deinit();
        }

        const path = args.items[1];

        var pool: thread.Pool = undefined;
        try pool.init(.{ .allocator = allocator });
        defer pool.deinit();

        try pool.spawn(spawnWorker, .{ allocator, path });
        debug.print("number of threads: {}\n", .{pool.threads.len});
    }
}
