const std = @import("std");
const debug = std.debug;
const heap = std.heap;
const mem = std.mem;
const fs = std.fs;
const math = std.math;

pub const WorkerConfig = struct {
    allocator: ?mem.Allocator = null,
};

pub const Worker = @This();

allocator: mem.Allocator,
list: std.ArrayList([]const u8),
is_leader: bool,

pub fn init(config: WorkerConfig, is_leader: bool) !Worker {
    return Worker{
        .allocator = config.allocator.?,
        .list = std.ArrayList([]const u8).init(config.allocator.?),
        .is_leader = is_leader,
    };
}

pub fn deinit(self: Worker) void {
    self.list.deinit();
}

pub fn run(self: *Worker, path: []const u8) !void {
    var dir = try fs.cwd().openDir(path, .{.iterate = true});
    defer dir.close();

    var it = dir.iterate();
    
    // //TODO: if this worker is leader, assign map or reduce work to idle worker

    while (try it.next()) |file| {
        const fname_with_path = try mem.concat(self.allocator, u8, &[_][]const u8{ path, file.name} );
        try self.add(fname_with_path);
    }

    try self.print();
}

fn add(self: *Worker, str: []const u8) !void {
    try self.list.append(str);
}

fn print(self: Worker) !void {
    for (self.list.items) | fname| {
        debug.print("Leader: {} - {s}\n", .{ self.is_leader, fname });
        const contents = try self.printFileContent(fname);
        defer self.allocator.free(contents);
        debug.print("{s}\n", .{contents});
    }
}

fn printFileContent(self: Worker, filename: []const u8) ![]u8 {
    var file_handle = try fs.cwd().openFile(filename, .{});
    defer file_handle.close();

    const contents = try file_handle.readToEndAlloc(self.allocator, math.maxInt(usize));

    return contents;
}